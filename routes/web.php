<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\{
    HomeController,
    BukuController,
    PenerbitController,
    PengadaanController,
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::prefix('admin')->name('admin.')->group( function() {

    Route::post('/buku/ajax', [BukuController::class, 'ajax'])->name('buku.ajax');
    Route::resource('buku', BukuController::class);;

    Route::post('/penerbit/ajax', [PenerbitController::class, 'ajax'])->name('penerbit.ajax');
    Route::resource('penerbit', PenerbitController::class);;

});

Route::post('/pengadaan/ajax', [PengadaanController::class, 'ajax'])->name('pengadaan.ajax');
Route::get('/pengadaan', [PengadaanController::class, 'index'])->name('pengadaan.index');
