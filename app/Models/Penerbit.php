<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Penerbit extends Model
{
    use HasFactory;

    protected $table = "penerbit";
    protected $primaryKey = "id_penerbit";
    public $incrementing = false;

    public $fillable = [
        'id_penerbit',
        'nama',
        'alamat',
        'kota',
        'telepon'
    ];


    public function buku(): HasMany
    {
        return $this->hasMany(Buku::class, 'id_penerbit', 'id_penerbit');
    }
}
