<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\Html\Builder;

use App\Models\{
    Buku,
    Penerbit
};

class PengadaanController extends Controller
{
    public function ajax(Request $request)
    {
        $getData = Buku::with('penerbit')->where('stok', '<' , 10)->get();

        return DataTables::of($getData)
        ->addColumn('action', function ($modal) {
            return '<a class="btn btn-sm btn-primary" href="tel:'. $modal->penerbit->telepon .'"><i class="fa fa-phone"></i> Kontak Penerbit</a>';
        })
        ->rawColumns([
            'action'
        ])
        ->make(true);
    }

    public function index(Request $request, Builder $builder)
    {
        $page = [
            'Pengadaan',
            'Pengadaan',
            'Pengadaan',
        ];

        $html = $builder
        ->columns([
            ['data' => "nama_buku", 'name' => "nama_buku", 'title' => 'Nama Buku'],
            ['data' => "stok", 'name' => "stok", 'title' => 'Stok'],
            ['data' => "penerbit.nama", 'name' => "penerbit", 'title' => 'Penerbit'],
            ['data' => "kategori", 'name' => "kategori", 'title' => 'Kategori'],
            ['data' => "harga", 'name' => "harga", 'title' => 'Harga'],
            ['data' => "action", 'name' => "action", 'title' => 'Action'],
        ])
        ->ajax([
            'url' => route('pengadaan.ajax'),
            'headers' => [
                'X-CSRF-TOKEN' => "$('meta[name=\"csrf-token\"]').attr('content')"
            ],
            'type' => 'POST'
        ])->parameters([
            "sScrollX" => '100%',
            'bAutoWidth' => false,
            "order" => [
                [0, 'ASC']
            ],
            'columnDefs' => [
                ['targets' => -1, 'width' => '100px']
            ],
            "serverSide" => true,
            'searchDelay' => 350,
            "lengthMenu" => [10, 25, 50, 100],
            'processing' => true
        ]);

        return view('pengadaan.index', [
            'page' => $page,
            'html' => $html,
        ]);
    }
}
