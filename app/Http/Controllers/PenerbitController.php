<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\Html\Builder;

use App\Models\{
    Buku,
    Penerbit
};

class PenerbitController extends Controller
{
    public function ajax(Request $request)
    {
        $getData = Penerbit::all();

        return DataTables::of($getData)
            ->addColumn('action', function ($model) {
                return '
            <div class="btn-group">
                <a href="' . route('admin.penerbit.show', $model->id_penerbit) . '" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
                <a href="' . route('admin.penerbit.edit', $model->id_penerbit) . '" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
            </div>
            ';
            })
            ->rawColumns([
                'action'
            ])
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Builder $builder)
    {
        $page = [
            'Admin',
            'Penerbit',
            'Data Penerbit',
        ];

        $html = $builder
        ->columns([
            ['data' => "id_penerbit", 'name' => "id_penerbit", 'title' => 'ID'],
            ['data' => "nama", 'name' => "nama", 'title' => 'Nama'],
            ['data' => "alamat", 'name' => "alamat", 'title' => 'Alamat'],
            ['data' => "kota", 'name' => "kota", 'title' => 'Kota'],
            ['data' => "telepon", 'name' => "telepon", 'title' => 'Telepon'],
            ['data' => "action", 'name' => "action", 'title' => 'Action'],
        ])
        ->ajax([
            'url' => route('admin.penerbit.ajax'),
            'headers' => [
                'X-CSRF-TOKEN' => "$('meta[name=\"csrf-token\"]').attr('content')"
            ],
            'type' => 'POST'
        ])->parameters([
            "sScrollX" => '100%',
            'bAutoWidth' => false,
            "order" => [
                [0, 'ASC']
            ],
            'columnDefs' => [
                ['targets' => -1, 'width' => '50px']
            ],
            "serverSide" => true,
            'searchDelay' => 350,
            "lengthMenu" => [10, 25, 50, 100],
            'processing' => true
        ]);

        return view('admin.penerbit.index', [
            'page' => $page,
            'html' => $html,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $page = [
            'Admin',
            'Penerbit',
            'Tambah Data Penerbit',
        ];

        return view('admin.penerbit.create', [
            'page'  => $page
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'id_penerbit' => 'required|unique:penerbit',
            'nama' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'telepon' => 'required'
        ]);

        Penerbit::create($validated);

        return redirect(route('admin.penerbit.index'))->with('success', 'Berhasil tambah data penerbit dengan ID: ' . $validated['id_penerbit']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Penerbit $penerbit)
    {
        $page = [
            'Admin',
            'Penerbit',
            'Detail Penerbit: ' . $penerbit->id_penerbit . '',
        ];

        return view('admin.penerbit.show', [
            'page'  => $page,
            'penerbit'  => $penerbit,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Penerbit $penerbit)
    {
        $page = [
            'Admin',
            'Penerbit',
            'Edit Penerbit: ' . $penerbit->id_penerbit . '',
        ];

        return view('admin.penerbit.edit', [
            'page'  => $page,
            'penerbit'  => $penerbit,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Penerbit $penerbit)
    {
        $rules = [
            'nama' => 'required',
            'alamat' => 'required',
            'kota' => 'required',
            'telepon' => 'required'
        ];

        if ($request->input('id_penerbit') != $penerbit->id_penerbit) {
            $rules['id_penerbit'] = 'required|unique:penerbit,id_penerbit';
        }

        $validated = $request->validate($rules);

        $penerbit->update($validated);

        return redirect(route('admin.penerbit.show', $penerbit->id_penerbit))->with('success', 'Berhasil edit data penerbit!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Penerbit $penerbit)
    {
        $penerbit->delete();

        return redirect(route('admin.penerbit.index'))->with('success', 'Berhasil hapus data penerbit!');
    }
}
