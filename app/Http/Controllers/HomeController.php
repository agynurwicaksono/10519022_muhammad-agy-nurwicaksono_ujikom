<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\Html\Builder;

class HomeController extends Controller
{
    public function index(Request $request, Builder $builder)
    {
        $page = [
            'Home',
            'Home',
            'Dashboard',
        ];

        $html = $builder
        ->columns([
            ['data' => "id_buku", 'name' => "id_buku", 'title' => 'ID'],
            ['data' => "kategori", 'name' => "kategori", 'title' => 'Kategori'],
            ['data' => "nama_buku_url", 'name' => "nama_buku_url", 'title' => 'Nama Buku'],
            ['data' => "harga", 'name' => "harga", 'title' => 'Harga'],
            ['data' => "stok", 'name' => "stok", 'title' => 'Stok'],
            ['data' => "nama_penerbit_url", 'name' => "nama_penerbit_url", 'title' => 'Penerbit'],
        ])
        ->ajax([
            'url' => route('admin.buku.ajax'),
            'headers' => [
                'X-CSRF-TOKEN' => "$('meta[name=\"csrf-token\"]').attr('content')"
            ],
            'type' => 'POST'
        ])->parameters([
            "sScrollX" => '100%',
            'bAutoWidth' => false,
            "order" => [
                [0, 'ASC']
            ],
            "serverSide" => true,
            'searchDelay' => 350,
            "lengthMenu" => [10, 25, 50, 100],
            'processing' => true
        ]);

        return view('home', [
            'page' => $page,
            'html' => $html
        ]);
    }
}
