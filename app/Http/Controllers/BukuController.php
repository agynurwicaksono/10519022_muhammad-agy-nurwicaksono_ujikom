<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\Html\Builder;

use App\Models\{
    Buku,
    Penerbit
};

class BukuController extends Controller
{
    public function ajax(Request $request)
    {
        $getData = Buku::with('penerbit')->get();

        return DataTables::of($getData)
        ->addColumn('nama_buku_url', function ($model) {
            return '<a href="' . route('admin.buku.show', $model->id_buku) . '">'. $model->nama_buku .'</a>';
        })
        ->addColumn('nama_penerbit_url', function ($model) {
            return '<a href="' . route('admin.penerbit.show', $model->id_penerbit) . '">'. $model->penerbit->nama .'</a>';
        })
        ->addColumn('action', function ($model){
            return '
            <div class="btn-group">
                <a href="' . route('admin.buku.show', $model->id_buku) . '" class="btn btn-sm btn-default"><i class="fa fa-eye"></i></a>
                <a href="' . route('admin.buku.edit', $model->id_buku) . '" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
            </div>
            ';
        })
        ->rawColumns([
            'action',
            'nama_buku_url',
            'nama_penerbit_url',
        ])
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, Builder $builder)
    {
        $page = [
            'Admin',
            'Buku',
            'Data Buku',
        ];

        $html = $builder
        ->columns([
            ['data' => "id_buku", 'name' => "id_buku", 'title' => 'ID'],
            ['data' => "kategori", 'name' => "kategori", 'title' => 'Kategori'],
            ['data' => "nama_buku", 'name' => "nama_buku", 'title' => 'Nama Buku'],
            ['data' => "harga", 'name' => "harga", 'title' => 'Harga'],
            ['data' => "stok", 'name' => "stok", 'title' => 'Stok'],
            ['data' => "penerbit.nama", 'name' => "penerbit", 'title' => 'Penerbit'],
            ['data' => "action", 'name' => "action", 'title' => 'Action'],
        ])
        ->ajax([
            'url' => route('admin.buku.ajax'),
            'headers' => [
                'X-CSRF-TOKEN' => "$('meta[name=\"csrf-token\"]').attr('content')"
            ],
            'type' => 'POST'
        ])->parameters([
            "sScrollX" => '100%',
            'bAutoWidth' => false,
            "order" => [
                [0, 'ASC']
            ],
            'columnDefs' => [
                ['targets' => -1, 'width' => '50px']
            ],
            "serverSide" => true,
            'searchDelay' => 350,
            "lengthMenu" => [10, 25, 50, 100],
            'processing' => true
        ]);

        return view('admin.buku.index', [
            'page' => $page,
            'html' => $html,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $page = [
            'Admin',
            'Buku',
            'Tambah Data Buku',
        ];

        $getPenerbit = Penerbit::all();

        return view('admin.buku.create', [
            'page'  => $page,
            'getPenerbit'  => $getPenerbit,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'id_buku' => 'required|unique:buku',
            'kategori' => 'required',
            'nama_buku' => 'required',
            'harga' => 'required|integer',
            'stok' => 'required|integer',
            'id_penerbit' => 'required|exists:penerbit,id_penerbit',
        ]);

        Buku::create($validated);

        return redirect(route('admin.buku.index'))->with('success', 'Berhasil tambah data buku dengan ID: ' . $validated['id_buku']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, Buku $buku)
    {
        $page = [
            'Admin',
            'Buku',
            'Detail Buku: '. $buku->id_buku .'',
        ];

        $getPenerbit = Penerbit::all();

        return view('admin.buku.show', [
            'page'  => $page,
            'buku'  => $buku,
            'getPenerbit'  => $getPenerbit,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Buku $buku)
    {
        $page = [
            'Admin',
            'Buku',
            'Edit Buku: ' . $buku->id_buku . '',
        ];

        $getPenerbit = Penerbit::all();

        return view('admin.buku.edit', [
            'page'  => $page,
            'buku'  => $buku,
            'getPenerbit'  => $getPenerbit,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Buku $buku)
    {
        $rules = [
            'kategori' => 'required',
            'nama_buku' => 'required',
            'harga' => 'required|integer',
            'stok' => 'required|integer',
            'id_penerbit' => 'required|exists:penerbit,id_penerbit',
        ];

        if ($request->input('id_buku') != $buku->id_buku) {
            $rules['id_buku'] ='required|unique:buku,id_buku';
        }

        $validated = $request->validate($rules);

        $buku->update($validated);

        return redirect(route('admin.buku.show', $buku->id_buku))->with('success', 'Berhasil edit data buku!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Buku $buku)
    {
        $buku->delete();

        return redirect(route('admin.buku.index'))->with('success', 'Berhasil hapus data buku!');
    }
}

