<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->string('id_buku', 10)->unique();
            $table->primary('id_buku');

            $table->string('kategori', 255);
            $table->string('nama_buku', 255);
            $table->integer('harga');
            $table->integer('stok');

            $table->string('id_penerbit', 10);
            $table->foreign('id_penerbit')->references('id_penerbit')->on('penerbit');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('buku');
    }
};
