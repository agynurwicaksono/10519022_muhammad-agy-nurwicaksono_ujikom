# UNIBOOKSTORE (10519022_Muhammad Agy Nurwicaksono_UJIKOM)

![Website Screenshot](screenshot-web.png)
## Informasi Mahasiswa

- Nama: Muhammad Agy Nurwicaksono
- Nomor Induk Mahasiswa (NIM): 10519022

## Deskripsi Proyek

Proyek ini adalah sebuah aplikasi web yang dibangun menggunakan framework Laravel. Tujuan dari proyek ini adalah untuk memenuhi Uji Kompetensi yang diselenggarakan oleh PT Chlorine Digital Media.

## Teknologi yang Digunakan

- Framework: Laravel
- Bahasa Pemrograman: PHP
- Database: MySQL
- Library dan Paket yang Digunakan:
    - AdminLTE v2 (Template)
    - Yajra Datatables
    - Swal (Sweet Alert)
## Fitur Aplikasi

Proyek ini memiliki beberapa fitur yang akan diimplementasikan, antara lain:

- Halaman Home, yang menampilkan data buku keseluruhan beserta penerbitnya.
- Halaman Admin yang terdiri dari:
    - Buku, berisi CRUD dari Tabel Buku.
    - Penerbit, berisi CRUD dari Tabel Penerbit.
- Halaman Pengadaan, yang menampilkan data buku yang memiliki stok kurang dari 10.
## Cara Menjalankan Proyek

Berikut adalah langkah-langkah untuk menjalankan proyek ini:

1. Pastikan PHP dan Composer sudah terinstall di komputer Anda.
1. Clone repositori ini ke dalam direktori lokal.
1. Buka terminal atau command prompt, lalu arahkan ke direktori proyek.
1. Jalankan perintah `composer install` untuk menginstal semua dependensi yang diperlukan.
1. Duplikat file `.env.example` dan ubah namanya menjadi `.env`. Konfigurasi file `.env` sesuai dengan pengaturan database Anda.
1. Jalankan perintah `php artisan key:generate` untuk menghasilkan kunci aplikasi unik.
1. Jalankan perintah `php artisan migrate` untuk menjalankan migrasi database.
1. Jalankan perintah `php artisan serve` untuk menjalankan server pengembangan Laravel.
1. Buka browser dan kunjungi `http://localhost:8000` untuk melihat proyek ini berjalan.
## Penjelasan Proyek

Proyek ini adalah program toko buku berbasis web dengan folder "UNIBOOKSTORE". Program ini memungkinkan pengguna untuk menjelajahi dan membeli buku-buku dari penerbit yang terdaftar. Selain itu, terdapat juga halaman admin untuk mengelola data buku dan penerbit, serta halaman pengadaan yang menampilkan daftar buku yang perlu segera dibeli berdasarkan sisa stok yang paling sedikit.

## Catatan

**Projek ini dikembangkan secara sepenuhnya oleh Agy**. Pastikan untuk mengikuti panduan dokumentasi resmi Laravel untuk informasi lebih lanjut tentang penggunaan framework dan fitur yang tersedia. 
