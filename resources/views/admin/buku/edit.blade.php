@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin.buku.show', $buku->id_buku) }}"
                        class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i></a> Edit Data Buku
                    {{ $buku->id_buku }}</h3>
            </div>

            <form action="{{ route('admin.buku.update', $buku->id_buku) }}" method="POST" id="form_edit">
                <div class="box-body">

                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label>ID Buku</label>
                        <input type="text" name="id_buku" class="form-control" value="{{ $buku->id_buku }}" required>
                    </div>

                    <div class="form-group">
                        <label>Kategori</label>
                        <input type="text" name="kategori" class="form-control" value="{{ $buku->kategori }}" required>
                    </div>

                    <div class="form-group">
                        <label>Nama Buku</label>
                        <input type="text" name="nama_buku" class="form-control" value="{{ $buku->nama_buku }}"
                            required>
                    </div>

                    <div class="form-group">
                        <label>Harga</label>
                        <input type="number" name="harga" class="form-control" value="{{ $buku->harga }}" required>
                    </div>

                    <div class="form-group">
                        <label>Stok</label>
                        <input type="number" name="stok" class="form-control" value="{{ $buku->stok }}" required>
                    </div>

                    <div class="form-group">
                        <label>Penerbit</label>
                        <select name="id_penerbit" class="form-control" required>
                            @foreach ($getPenerbit as $penerbit)
                            <option value="{{ $penerbit->id_penerbit }}"
                                {{ $buku->penerbit->id_penerbit == $penerbit->id_penerbit ? 'selected' : '' }}>
                                {{ $penerbit->nama }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="box-footer">
                    <div>
                        <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i
                                class="fa fa-trash"></i> Hapus Buku</button>
                        <button type="submit" class="btn btn-success pull-right"><i
                                class="fa fa-save"></i> Simpan Data Buku</button>
                    </div>
                </div>
            </form>


            <form action="{{ route('admin.buku.destroy', $buku->id_buku) }}" method="POST" id="form_delete">
                @csrf
                @method('DELETE')
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function confirmDelete(){
        Swal.fire({
            title: `Hapus Data Buku?`,
            text: `Semua data yang sudah terpilih akan hilang`,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya"
        }).then(function(result) {
            if (result.isConfirmed) {
                $("#form_delete").submit();
            }
        });
    };
</script>
@endsection
