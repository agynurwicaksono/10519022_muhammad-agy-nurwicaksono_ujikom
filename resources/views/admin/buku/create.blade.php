@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin.buku.index') }}" class="btn btn-default btn-sm"><i
                            class="fa fa-arrow-left"></i></a> Tambah Data Buku</h3>
            </div>
            <form action="{{ route('admin.buku.store') }}" method="POST" id="form_add">

                <div class="box-body">

                    @csrf

                    <div class="form-group">
                        <label>ID Buku</label>
                        <input type="text" name="id_buku" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Kategori</label>
                        <input type="text" name="kategori" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Nama Buku</label>
                        <input type="text" name="nama_buku" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Harga</label>
                        <input type="number" name="harga" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Stok</label>
                        <input type="number" name="stok" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Penerbit</label>
                        <select name="id_penerbit" class="form-control" required>
                            <option value="">== PILIH PENERBIT ==</option>
                            @foreach ($getPenerbit as $penerbit)
                            <option value="{{ $penerbit->id_penerbit }}">{{ $penerbit->nama }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="box-footer">
                    <div>
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Tambah Data
                            Buku</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
