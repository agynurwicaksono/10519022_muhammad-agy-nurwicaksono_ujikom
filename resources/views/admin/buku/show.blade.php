@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

        @if($message = Session::get('success'))
            <div class="alert alert-success">
                <h4>{{ $message }}</h4>
            </div>
        @endif

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Data Buku</h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('admin.buku.edit', $buku->id_buku) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Edit Buku</a>
                </div>
            </div>

            <div class="box-body">

                <div class="form-group">
                  <label>ID Buku</label>
                  <input type="text" name="id_buku" class="form-control" value="{{ $buku->id_buku }}" disabled>
                </div>

                <div class="form-group">
                  <label>Kategori</label>
                  <input type="text" name="kategori" class="form-control" value="{{ $buku->kategori }}" disabled>
                </div>

                <div class="form-group">
                  <label>Nama Buku</label>
                  <input type="text" name="nama_buku" class="form-control" value="{{ $buku->nama_buku }}" disabled>
                </div>

                <div class="form-group">
                  <label>Harga</label>
                  <input type="text" name="harga" class="form-control" value="{{ $buku->harga }}" disabled>
                </div>

                <div class="form-group">
                  <label>Stok</label>
                  <input type="text" name="stok" class="form-control" value="{{ $buku->stok }}" disabled>
                </div>

                <div class="form-group">
                  <label>Penerbit</label>
                  <input type="text" name="penerbit" class="form-control" value="{{ $buku->penerbit->nama }}" disabled>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
