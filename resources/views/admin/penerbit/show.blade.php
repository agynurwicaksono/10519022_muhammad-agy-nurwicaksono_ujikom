@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

        @if($message = Session::get('success'))
            <div class="alert alert-success">
                <h4>{{ $message }}</h4>
            </div>
        @endif

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Data Penerbit</h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('admin.penerbit.edit', $penerbit->id_penerbit) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Edit Penerbit</a>
                </div>
            </div>

            <div class="box-body">

                <div class="form-group">
                  <label>ID Penerbit</label>
                  <input type="text" name="id_penerbit" class="form-control" value="{{ $penerbit->id_penerbit }}" disabled>
                </div>

                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" value="{{ $penerbit->nama }}" disabled>
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control" rows="4" disabled>{{ $penerbit->alamat }}</textarea>
                </div>

                <div class="form-group">
                    <label>Kota</label>
                    <input type="text" name="kota" class="form-control" value="{{ $penerbit->kota }}" disabled>
                </div>

                <div class="form-group">
                    <label>Telepon</label>
                    <input type="text" name="telepon" class="form-control" value="{{ $penerbit->telepon }}" disabled>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
