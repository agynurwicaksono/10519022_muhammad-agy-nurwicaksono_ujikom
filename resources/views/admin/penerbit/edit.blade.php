@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin.penerbit.show', $penerbit->id_penerbit) }}"
                        class="btn btn-default btn-sm"><i class="fa fa-arrow-left"></i></a> Edit Data Penerbit
                    {{ $penerbit->id_penerbit }}</h3>
            </div>

            <form action="{{ route('admin.penerbit.update', $penerbit->id_penerbit) }}" method="POST" id="form_edit">
                <div class="box-body">

                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label>ID Penerbit</label>
                        <input type="text" name="id_penerbit" class="form-control" value="{{ $penerbit->id_penerbit }}" required>
                    </div>

                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" value="{{ $penerbit->nama }}" required>
                    </div>

                    <div class="form-group">
                        <label>Nama</label>
                        <textarea name="alamat" class="form-control" rows="4" required>{{ $penerbit->alamat }}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Kota</label>
                        <input type="text" name="kota" class="form-control" value="{{ $penerbit->kota }}" required>
                    </div>

                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" name="telepon" class="form-control" value="{{ $penerbit->telepon }}" required>
                    </div>

                </div>
                <div class="box-footer">
                    <div>
                        <button type="button" onclick="confirmDelete()" class="btn btn-danger"><i
                                class="fa fa-trash"></i> Hapus Penerbit</button>
                        <button type="submit" class="btn btn-success pull-right"><i
                                class="fa fa-save"></i> Simpan Data Penerbit</button>
                    </div>
                </div>
            </form>


            <form action="{{ route('admin.penerbit.destroy', $penerbit->id_penerbit) }}" method="POST" id="form_delete">
                @csrf
                @method('DELETE')
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function confirmDelete(){
        Swal.fire({
            title: `Hapus Data Penerbit?`,
            text: `Semua data yang sudah terpilih akan hilang`,
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya"
        }).then(function(result) {
            if (result.isConfirmed) {
                $("#form_delete").submit();
            }
        });
    };
</script>
@endsection
