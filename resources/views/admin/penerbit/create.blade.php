@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><a href="{{ route('admin.penerbit.index') }}" class="btn btn-default btn-sm"><i
                            class="fa fa-arrow-left"></i></a> Tambah Data Penerbit</h3>
            </div>
            <form action="{{ route('admin.penerbit.store') }}" method="POST" id="form_add">

                <div class="box-body">

                    @csrf

                    <div class="form-group">
                        <label>ID Penerbit</label>
                        <input type="text" name="id_penerbit" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" rows="4" required></textarea>
                    </div>

                    <div class="form-group">
                        <label>Kota</label>
                        <input type="text" name="kota" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" name="telepon" class="form-control" required>
                    </div>

                </div>
                <div class="box-footer">
                    <div>
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Tambah Data
                            Penerbit</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
