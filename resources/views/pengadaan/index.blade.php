@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">

        @if($message = Session::get('success'))
            <div class="alert alert-success">
                <h4>{{ $message }}</h4>
            </div>
        @endif

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Buku Yang Perlu Dibeli (Stok Dibawah 10)</h3>
            </div>

            <div class="box-body">
                {!! $html->table(['class' => 'table table-bordered table-hover'], true) !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
{!! $html->scripts() !!}
@endsection
