@include('layouts._header')

<div class="wrapper">

    @include('layouts._main-header')

    @include('layouts._sidebar')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>
                {{ $page[2] }}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                @if ($page[0] == "Admin")
                    <li>{{ $page[1] }}</li>
                @endif
                <li class="active">{{ $page[2] }}</li>
            </ol>
        </section>

        <section class="content">

            @yield('content')

        </section>

    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2023 <a href="#">UniBookStore</a>.</strong> All rights
        reserved.
    </footer>
</div>

@include('layouts._footer')
