<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('dist/img/user.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Administrator</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                            class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>


        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <li class="{{ $page[0] == 'Home' ? 'active' : '' }}">
                <a href="{{ route('home') }}">
                    <i class="fa fa-dashboard"></i> <span>Home</span>
                </a>
            </li>

            <li class="{{ $page[0] == 'Admin' ? 'active menu-open' : '' }} treeview">

                <a href="#">
                    <i class="fa fa-cogs"></i> <span>Admin</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $page[1] == 'Buku' ? 'active' : '' }}"><a href="{{ route('admin.buku.index') }}"><i class="fa fa-circle-o"></i> Buku</a></li>
                    <li class="{{ $page[1] == 'Penerbit' ? 'active' : '' }}"><a href="{{ route('admin.penerbit.index') }}"><i class="fa fa-circle-o"></i> Penerbit</a></li>
                </ul>

            </li>

            <li class="{{ $page[0] == 'Pengadaan' ? 'active' : '' }}">
                <a href="{{ route('pengadaan.index') }}">
                    <i class="fa fa-th"></i> <span>Pengadaan</span>
                </a>
            </li>

        </ul>
    </section>

</aside>
